# LEMP Playbook

Simple set of roles I use for provisioning a Debian Jessie LEMP stack. Currently includes:

* PHP 7 FPM and CLI (dotdeb)
* Nginx (dotdeb)
* Tmux 2 (jessie-backports)
* Certbot (jessie-backports)
* ZSH, Git, Curl, Htop (stock)

The nginx default conf is configured to use the hostname(s) from your inventory file, and assumes you've already run certbot to install the certs. Uncomment the SSL statements in default.conf before you run certbot for the first time.

## Notes
The site.yml also requires a user with sudo privileges for most of the tasks. Make sure your user has sudo privileges.

## Running
Clone the repo and adjust site.yml to match your remote_user, then create an ansible inventory file with your hosts. Then run:

~~~~
ansible-playbook -i hosts site.yml --ask-sudo-pass
~~~~

Where hosts is the inventory file. If your user needs to enter their password to escalate via sudo, you'll need the ask-sudo-pass switch from the example.

## Things to come
- We're missing the M (MySQL, MariaDB) in the LEMP stack. Oops!
- The default fail2ban conf is massive. A more concise version wouldn't hurt.
- Speaking of ssh, perhaps managing the sshd_conf too.
- Conditional fallback nginx conf if the certbot certs don't exist would be cool